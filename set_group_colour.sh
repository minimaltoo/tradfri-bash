#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a group ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

if [ -z "${2}" ]; then
  echo "ERROR: Need a colour (cold|mid|warm)"
  exit 1
fi

case "${2}" in
  cold) MOOD_VALUE=${PRESET_FOCUS}
  ;;
  mid) MOOD_VALUE=${PRESET_EVERYDAY}
  ;;
  warm) MOOD_VALUE=${PRESET_RELAX}
  ;;
esac

if [ -z "${MOOD_VALUE}" ]; then
  echo "ERROR: Can't find a matching mood value"
  exit 1
fi

set_group_colour ${DEVICE} ${MOOD_VALUE}
