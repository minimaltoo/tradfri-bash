#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

for BULB in ${SERIES_LOUNGE[*]}; do
  set_off ${BULB}
done

for BULB in ${SERIES_UPSTAIRS_HALL[*]}; do
  set_polite_brightness ${BULB} ${LAMP_MIN}
done
