#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

for BULB in ${SERIES_MASTER_BEDROOM[*]} ${SERIES_UPSTAIRS_HALL[*]} ${SERIES_STUDY[*]} ${SERIES_LOUNGE[*]} ${SERIES_ROBIN_BEDROOM[*]} ${SERIES_KITCHEN[*]}; do
  set_colour ${BULB} ${COLOUR_WARM}
done

for BULB in ${SERIES_LOUNGE[*]} ${SERIES_MASTER_BEDROOM[*]} ${SERIES_STUDY[*]} ${SERIES_ROBIN_BEDROOM[*]} ${SERIES_KITCHEN[*]}; do
  set_polite_brightness ${BULB} 160
done

for BULB in ${SERIES_UPSTAIRS_HALL[*]}; do
  set_polite_brightness ${BULB} 100
done
