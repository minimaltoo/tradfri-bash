#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

set_bright 255

for HEX in efd275 f1e0b5 f2eccf f5faf6; do
  set_colour ${HEX}
  sleep 1
done

